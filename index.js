const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./db')

const app = express()
const jsonParser = bodyParser.json()

app.get('/', async(req, res) => {
	try{
		const data = await pool.query("SELECT * FROM karyawan")
		res.send(data.rows)
	} catch {
		res.status(500).send('Error Occurs')
	}
})

app.post('/', jsonParser, async(req, res) => {
	try {
		await pool.query("INSERT INTO karyawan (nama, alamat) VALUES ($1, $2)",[req.body.nama, req.body.alamat])
		res.status(201).send("Data Inserted")
	} catch (error) { 
		res.status(500).send("Kesalahan: Data Tidak Masuk")
	}
}) 

app.get('/setup', async(req, res) => {
	try{
		await pool.query("CREATE TABLE karyawan( id SERIAL PRIMARY KEY, nama VARCHAR(100), alamat VARCHAR(100) )")
		res.send("DB Created : Table Created")
	} catch (error){
		res.status(500).send("Error Occurs: Error DB not Create")
	}
})

app.listen(3000, (req, res) =>{
	console.log("Server is Already Running on port 3000")
})
